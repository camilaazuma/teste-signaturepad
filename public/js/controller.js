angular.module('app').controller('mainController', function ($scope, $timeout) {

	// Signature
		var canvas = document.querySelector("canvas");
		
		if(!canvas.supportsToDataURL()){
			alert('electronicSignature.errorUnsupported');
			return;
		}
	
		var signaturePad;
	
		function setSignaturePad(){
			// Timeout para instância do plugin (compatibilidade com Androids antigos)
			$timeout(function(){
				signaturePad = new SignaturePad(canvas,{
					onEnd: function(){
						$scope.$apply();
					}
				});
	
	
				// Adjust canvas coordinate space taking into account pixel ratio,
				// to make it look crisp on mobile devices.
				// This also causes canvas to be cleared.
				var resizeCanvas = function() {
					// When zoomed out to less than 100%, for some very strange reason,
					// some browsers report devicePixelRatio as less than 1
					// and only part of the canvas is cleared then.
					var ratio =  Math.max(window.devicePixelRatio || 1, 1);
					canvas.width = canvas.offsetWidth * ratio;
					canvas.height = canvas.offsetHeight * ratio;
					canvas.getContext("2d").scale(ratio, ratio);
	
					signaturePad.clear();
	
					if(!$scope.$$phase) {
						$scope.$apply();
					}
				};
	
				window.onresize = resizeCanvas;
				resizeCanvas();
			}, 100);
		};
		setSignaturePad();
	
		var resizeCanvas = function() {
			// When zoomed out to less than 100%, for some very strange reason,
			// some browsers report devicePixelRatio as less than 1
			// and only part of the canvas is cleared then.
			var ratio =  Math.max(window.devicePixelRatio || 1, 1);
			canvas.width = canvas.offsetWidth * ratio;
			canvas.height = canvas.offsetHeight * ratio;
			canvas.getContext("2d").scale(ratio, ratio);
	
			signaturePad.clear();
	
			if(!$scope.$$phase) {
				$scope.$apply();
			}
		};
	
		// Assinatura
		$scope.clearSignature = function(){
			signaturePad.clear();
		};
	
	});
	